package com.equus.asinus.infra.rest;


import com.equus.asinus.infra.adapter.UserServiceElasticAdapter;
import com.equus.asinus.infra.service.BookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@Slf4j
public class ElasticResource {
    @Autowired
    BookService bookService;
    @Autowired
    UserServiceElasticAdapter userServiceElasticAdapter;
    //@Autowired
   // TokenRepository redisRepository;

    @GetMapping("/search")
    public String redis() {
        log.error("eee  "+userServiceElasticAdapter.findByUsername("a@a.com").get());
        return "true";
    }



}
