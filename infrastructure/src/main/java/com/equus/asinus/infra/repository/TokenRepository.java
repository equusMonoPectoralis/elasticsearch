package com.equus.asinus.infra.repository;


import com.equus.asinus.infra.entity.TokenRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public class TokenRepository {

    @Autowired
    private RedisTemplate<String, TokenRedis> redisTemplate;

    public void save(TokenRedis user) {
        redisTemplate.opsForValue().set(user.getUserName(), user);
    }

    public List<TokenRedis> findAll() {
        List<TokenRedis> userList = new ArrayList<>();
        //sudo docker run --rm -p 6379:6379 -d --name redis-1 redis redis-server
        Set<String> keys = redisTemplate.keys("*");
        if (keys!= null){
            keys.forEach(k->userList.add(findById(k)));
        }
        return userList;
    }

    public TokenRedis findById(String key) {
        return redisTemplate.opsForValue().get(key);
    }
}
