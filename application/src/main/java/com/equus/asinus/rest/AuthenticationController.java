package com.equus.asinus.rest;

import com.equus.asinus.adapter.UserServiceAdapter;
import com.equus.asinus.domain.models.ERole;
import com.equus.asinus.domain.models.Role;
import com.equus.asinus.domain.models.User;
import com.equus.asinus.payload.AuthPayload;
import com.equus.asinus.payload.ResponsePayload;
import com.equus.asinus.payload.SignUpPayload;
import com.equus.asinus.utils.JwtTokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/auth")
@Slf4j
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserServiceAdapter userDetailsService;

    @Autowired
    private PasswordEncoder encoder;

    @PostMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> createAuthenticationToken(@RequestBody AuthPayload authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails);

        // Add redis code
        //userDetailsService.saveTokenOnRedis(new TokenRedis(authenticationRequest.getUsername(), token));
        return ResponseEntity.ok(new ResponsePayload(Boolean.TRUE,token));
    }

    @PostMapping(value = "/signup", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponsePayload> signUp(@RequestBody SignUpPayload signUpRequest)  {
        Optional<User> userOptional = userDetailsService.findByUsername(signUpRequest.getUsername());
        if (userOptional.isPresent()) {
            return ResponseEntity.ok(new ResponsePayload(Boolean.FALSE,"Username is already taken!"));
        }
        User user = new User();
        try {
            BeanUtils.copyProperties(signUpRequest, user);
            String password = encoder.encode(signUpRequest.getPassword());
            user.setPassword(password);

            Set<Role> roles = new HashSet<>();
            roles.add(new Role(1, ERole.ROLE_USER));
            user.setAuthorities(roles);
            user.setUserId(UUID.randomUUID().toString());
            user.setCredentialsNonExpired(true);
            user.setAccountNonLocked(true);
            user.setAccountNonExpired(true);
            user.setEnabled(true);
            log.error(user.toString());
            userDetailsService.addUser(user);
        }catch(Exception e){
            e.printStackTrace();
            return ResponseEntity.ok(new ResponsePayload(Boolean.FALSE,"Error while creating user!"));
        }
        return ResponseEntity.ok(new ResponsePayload(Boolean.TRUE,user));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        }catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}