package com.equus.asinus.adapter;

import com.equus.asinus.security.config.UserDetailsImpl;
import com.equus.asinus.api.UserInterface;
import com.equus.asinus.domain.interfaces.port.server.UserPersistencePort;
import com.equus.asinus.domain.models.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class UserServiceAdapter implements UserInterface, UserDetailsService {

    private final UserPersistencePort userPersistencePort;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    public UserServiceAdapter(UserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }

    @Override public void addUser(User user) throws Exception {
        userPersistencePort.addUser(user);
    }

    @Override public void deleteUser(User user) {
        userPersistencePort.deleteUser(user);
    }

    @Override public List<User> getUsers() {
        return userPersistencePort.getUsers();
    }

    @Override public User getUserById(String id) {
        return null;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userPersistencePort.findByUsername(username);
    }

    @Override
    public UserDetailsImpl loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userOptional = userPersistencePort.findByUsername(username);
        return userOptional.map(UserDetailsImpl::build).orElseThrow(()->new UsernameNotFoundException("User not found with username: " + username));
    }
}
