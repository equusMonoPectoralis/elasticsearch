package com.equus.asinus.payload;

import com.equus.asinus.domain.models.Role;
import com.equus.asinus.domain.models.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class SignUpPayload {
    private String    firstName;
    private String    lastName;
    private String    password;
    private String    username;
    private LocalDate birthday;
    private String  email;

}