# Hexagonal Architecture Example
A spring-boot based example of hexagonal architecture.

![hexagonal architecture](hexagonal.svg)
## Getting Started
    git clone https://gitlab.com/equusMonoPectoralis/elasticsearch.git

## Docker
### Launch elastic-search

    sudo docker network create somenetwork

    sudo docker run -d --name elasticsearch --net somenetwork -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.6.2

### Launch Redis

    sudo docker run --rm -p 4025:6379 -d --name redis-1 redis redis-server





