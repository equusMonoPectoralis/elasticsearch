package com.equus.asinus.domain.interfaces.port.server;

import com.equus.asinus.domain.models.Book;

import java.util.List;

public interface BookPersistencePort {

    void addBook(Book book);

    void deleteBook(Book book);

    List<Book> getBooks();

    Book getBookById(String id);
}