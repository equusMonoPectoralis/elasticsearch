package com.equus.asinus.domain.interfaces.port.server;

import com.equus.asinus.domain.models.User;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

public interface UserPersistencePort {

    void addUser(User user) throws Exception;

    void deleteUser(User user);

    List<User> getUsers();

    User getUserById(String id);

    Optional<User> findByUsername(String username);
}
