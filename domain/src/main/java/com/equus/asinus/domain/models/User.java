package com.equus.asinus.domain.models;

import lombok.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String    userId;
    private String    firstName;
    private String    lastName;
    private String    password;
    private String    username;
    private LocalDate birthday;
    private String email;
    private Set<Role> authorities = new HashSet<>();
    private boolean   accountNonExpired;
    private boolean   accountNonLocked;
    private boolean   credentialsNonExpired;
    private boolean   enabled;

}
